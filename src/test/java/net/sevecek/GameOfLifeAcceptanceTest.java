package net.sevecek;

import org.junit.jupiter.api.Test;
import net.sevecek.gameoflife.GameOfLife;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GameOfLifeAcceptanceTest {

    @Test
    void multipleGenerationsFromGivenState() {
        String initialState = "" +
                "..XXX..\n" +
                "..X.X..\n" +
                "..X.X..\n" +
                "...X...\n" +
                "X.XXX..\n" +
                ".X.X.X.\n" +
                "...X..X\n" +
                "..X.X..\n" +
                "..X.X..";
        GameOfLife game = new GameOfLife(initialState);

        for (int i = 0; i < 4; i++) {
            game = game.makeNextGeneration();
        }

        String expectedState = "" +
                "...............\n" +
                "...............\n" +
                ".......X.......\n" +
                "......XXX......\n" +
                "......XXXX.....\n" +
                "........X.X....\n" +
                ".........X.....\n" +
                "...............\n" +
                "...............\n" +
                ".........X.....\n" +
                "......X...X....\n" +
                "......XXXXX....\n" +
                ".......XXX.....\n" +
                "...............\n" +
                "...............\n" +
                "...............\n" +
                "...............\n";
        assertEquals(expectedState, game.toString());
    }

    @Test
    void initialGeneratedState() {
        GameOfLife game = new GameOfLife(10, 10);

        String expectedState = "" +
                "XX.XX.X.XX\n" +
                "...XXXX.X.\n" +
                ".....X..X.\n" +
                "XX..X...XX\n" +
                "XX.XXXXXX.\n" +
                "X.XXXX.X..\n" +
                "X.XXX.....\n" +
                ".X.X.X.XXX\n" +
                "XXXXX.....\n" +
                "X...X.XXXX\n";
        assertEquals(expectedState, game.toString());
    }

    @Test
    void multipleGenerationsFromGeneratedState() {
        GameOfLife game = new GameOfLife(10, 10);

        for (int i = 0; i < 3; i++) {
            game = game.makeNextGeneration();
        }

        String expectedState = "" +
                "................\n" +
                "................\n" +
                "................\n" +
                "......XXX.XXX...\n" +
                ".......XX.......\n" +
                "....XX.X........\n" +
                "...XXXX......X..\n" +
                "..X..XX...X.X...\n" +
                ".X..X....XXX....\n" +
                "..XX.....XXX....\n" +
                "...X.......X....\n" +
                "........X..X....\n" +
                "............X...\n" +
                ".........X.X....\n" +
                "..........X.....\n" +
                "................\n";
        assertEquals(expectedState, game.toString());
    }
}
