package net.sevecek.gameoflife;

import java.util.Random;

public class GameOfLife {

    private final boolean[][] grid;
    private final int width;
    private final int height;

    public GameOfLife(int width, int height) {
        this.width = width;
        this.height = height;
        this.grid = new boolean[height][width];

        initialize();
    }

    public GameOfLife(String initialState) {
        this.height = initialState.replaceAll("[^\n]", "").length() + 1;
        this.width = initialState.replaceAll("(?ms)^(.*?)$.*", "$1").length();
        this.grid = new boolean[height][width];

        int pos = 0;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                grid[y][x] = initialState.charAt(pos++) == 'X';
            }
            pos = initialState.indexOf('\n', pos) + 1;
        }
    }

    private GameOfLife(int width, int height, boolean[][] grid) {
        this.width = width;
        this.height = height;
        this.grid = grid;
    }

    private void initialize() {
        Random generator = new Random(0L);

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                grid[y][x] = generator.nextBoolean();
            }
        }
    }

    public GameOfLife makeNextGeneration() {
        int newWidth = width + 2;
        int newHeight = height + 2;
        boolean[][] newGrid = new boolean[newHeight][newWidth];

        for (int y = 0; y < newHeight; y++) {
            for (int x = 0; x < newWidth; x++) {
                newGrid[y][x] = calculateCell(x - 1, y - 1);
            }
        }

        return new GameOfLife(newWidth, newHeight, newGrid);
    }

    private boolean calculateCell(int x, int y) {
        int liveNeighbors = calculateLiveNeighbors(x, y);

        if (isCellLive(x, y)) {
            return liveNeighbors == 2 || liveNeighbors == 3;
        } else {
            return liveNeighbors == 3;
        }
    }

    private boolean isCellLive(int x, int y) {
        return x >= 0 && x < width && y >= 0 && y < height && grid[y][x];
    }

    private int calculateLiveNeighbors(int x, int y) {
        int liveNeighbors = 0;
        if (isCellLive(x, y - 1)) liveNeighbors++;
        if (isCellLive(x + 1, y - 1)) liveNeighbors++;
        if (isCellLive(x + 1, y)) liveNeighbors++;
        if (isCellLive(x + 1, y + 1)) liveNeighbors++;
        if (isCellLive(x, y + 1)) liveNeighbors++;
        if (isCellLive(x - 1, y + 1)) liveNeighbors++;
        if (isCellLive(x - 1, y)) liveNeighbors++;
        if (isCellLive(x - 1, y - 1)) liveNeighbors++;
        return liveNeighbors;
    }

    public void print() {
        System.out.println("Grid " + width + "x" + height);

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                System.out.print(formatCell(x, y) + " ");
            }
            System.out.print("    ");
            for (int x = 0; x < width; x++) {
                System.out.print(calculateLiveNeighbors(x, y) + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                String cell = formatCell(x, y);
                output.append(cell);
            }
            output.append('\n');
        }

        return output.toString();
    }

    private String formatCell(int x, int y) {
        return isCellLive(x, y) ? "X" : ".";
    }
}
