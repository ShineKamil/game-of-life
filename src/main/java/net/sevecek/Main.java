package net.sevecek;

import net.sevecek.gameoflife.GameOfLife;

public class Main {

    public static void main(String[] args) {
        //GameOfLife game = new GameOfLife(10, 10);
        String initialState = "" +
                "..XXX..\n" +
                "..X.X..\n" +
                "..X.X..\n" +
                "...X...\n" +
                "X.XXX..\n" +
                ".X.X.X.\n" +
                "...X..X\n" +
                "..X.X..\n" +
                "..X.X..";
        GameOfLife game = new GameOfLife(initialState);
        for (int i = 0; i < 4; i++) {
            game.print();
            game = game.makeNextGeneration();
        }
        game.print();
    }
}
